/**
 * 
 */
package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Simple Icy plugin library for the WebcamCapture library
 * 
 * @author Stephane
 */
public class WebcamCaptureLibraryPlugin extends Plugin implements PluginLibrary
{
    // just to encapsulate the WebcamCapture library
}
